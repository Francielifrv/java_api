package com.puc.pos.lab01.web.rest;

import com.puc.pos.lab01.JavaApiApp;
import com.puc.pos.lab01.domain.Customer;
import com.puc.pos.lab01.repository.CustomerRepository;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CustomerResource REST controller.
 *
 * @see CustomerResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JavaApiApp.class)
@WebAppConfiguration
@IntegrationTest
public class CustomerResourceIntTest {

    private static final String NAME = "Anna";
    private static final String UPDATED_NAME = "Joana";
    private static final String SITE = "www.site.com";
    private static final String UPDATED_SITE = "www.anothersite.com";
    private static final String CNPJ = "12345";
    private static final String UPDATED_CNPJ = "09876";
    private static final String ADDRESS = "AAAAA";
    private static final String UPDATED_ADDRESS = "BBBBB";

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restCustomerMockMvc;

    private Customer customer;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CustomerResource customerResource = new CustomerResource();
        ReflectionTestUtils.setField(customerResource, "customerRepository", customerRepository);
        this.restCustomerMockMvc = MockMvcBuilders.standaloneSetup(customerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        customerRepository.deleteAll();
        customer = new Customer();
        customer.setName(NAME);
        customer.setSite(SITE);
        customer.setCnpj(CNPJ);
        customer.setAddress(ADDRESS);
    }

    @Test
    public void createCustomer() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer

        restCustomerMockMvc.perform(post("/api/customers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customer)))
                .andExpect(status().isCreated());

        // Validate the Customer in the database
        List<Customer> customers = customerRepository.findAll();
        assertThat(customers).hasSize(databaseSizeBeforeCreate + 1);
        Customer testCustomer = customers.get(customers.size() - 1);
        assertThat(testCustomer.getName()).isEqualTo(NAME);
        assertThat(testCustomer.getSite()).isEqualTo(SITE);
        assertThat(testCustomer.getCnpj()).isEqualTo(CNPJ);
        assertThat(testCustomer.getAddress()).isEqualTo(ADDRESS);
    }

    @Test
    public void getAllCustomers() throws Exception {
        // Initialize the database
        customerRepository.save(customer);

        // Get all the customers
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(NAME.toString())))
                .andExpect(jsonPath("$.[*].site").value(hasItem(SITE.toString())))
                .andExpect(jsonPath("$.[*].cnpj").value(hasItem(CNPJ.toString())))
                .andExpect(jsonPath("$.[*].address").value(hasItem(ADDRESS.toString())));
    }

    @Test
    public void getCustomer() throws Exception {
        // Initialize the database
        customerRepository.save(customer);

        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", customer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(customer.getId()))
            .andExpect(jsonPath("$.name").value(NAME.toString()))
            .andExpect(jsonPath("$.site").value(SITE.toString()))
            .andExpect(jsonPath("$.cnpj").value(CNPJ.toString()))
            .andExpect(jsonPath("$.address").value(ADDRESS.toString()));
    }

    @Test
    public void getCustomerWithDetails() throws Exception {
        customerRepository.save(customer);

        String getUserRequest = "/api/customers/q?name=" + customer.getName() + "&site=" + customer.getSite() +
            "&cnpj=" + customer.getCnpj() + "&address=" + customer.getAddress();
        restCustomerMockMvc.perform((get(getUserRequest)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(customer.getId()))
            .andExpect(jsonPath("$.name").value(customer.getName()))
            .andExpect(jsonPath("$.site").value(customer.getSite()))
            .andExpect(jsonPath("$.cnpj").value(customer.getCnpj()))
            .andExpect(jsonPath("$.address").value(customer.getAddress()));
    }

    @Test
    public void return404WhenUserDetailsDoNotMatch() throws Exception {
        customerRepository.save(customer);

        String getUserRequest = "/api/customers/q?name=" + "Jon Doe" + "&site=" + customer.getSite() +
            "&cnpj=" + customer.getCnpj() + "&address=" + customer.getAddress();
        restCustomerMockMvc.perform((get(getUserRequest)))
            .andExpect(status().isNotFound());
    }

    @Test
    public void getCustomerBetweenIds() throws Exception {
        Customer aCustomer = new Customer();
        aCustomer.setName("Jon Doe");
        aCustomer.setAddress(UPDATED_ADDRESS);
        aCustomer.setCnpj(UPDATED_CNPJ);
        aCustomer.setSite(UPDATED_SITE);

        Customer anotherCustomer = new Customer();
        anotherCustomer.setName("Jana Doe");
        anotherCustomer.setAddress(ADDRESS);
        anotherCustomer.setCnpj(CNPJ);
        anotherCustomer.setSite(SITE);

        customerRepository.save(customer);
        customerRepository.save(aCustomer);
        customerRepository.save(customer);

        restCustomerMockMvc.perform((get("/api/customers/{fromId}/{toId}", customer.getId(), anotherCustomer.getId())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void getNonExistingCustomer() throws Exception {
        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateCustomer() throws Exception {
        // Initialize the database
        customerRepository.save(customer);

        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Update the customer
        Customer updatedCustomer = new Customer();
        updatedCustomer.setId(customer.getId());
        updatedCustomer.setName(UPDATED_NAME);
        updatedCustomer.setSite(UPDATED_SITE);
        updatedCustomer.setCnpj(UPDATED_CNPJ);
        updatedCustomer.setAddress(UPDATED_ADDRESS);

        restCustomerMockMvc.perform(put("/api/customers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedCustomer)))
                .andExpect(status().isOk());

        // Validate the Customer in the database
        List<Customer> customers = customerRepository.findAll();
        assertThat(customers).hasSize(databaseSizeBeforeUpdate);
        Customer testCustomer = customers.get(customers.size() - 1);
        assertThat(testCustomer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomer.getSite()).isEqualTo(UPDATED_SITE);
        assertThat(testCustomer.getCnpj()).isEqualTo(UPDATED_CNPJ);
        assertThat(testCustomer.getAddress()).isEqualTo(UPDATED_ADDRESS);
    }

    @Test
    public void deleteCustomer() throws Exception {
        // Initialize the database
        customerRepository.save(customer);
        int databaseSizeBeforeDelete = customerRepository.findAll().size();

        // Get the customer
        restCustomerMockMvc.perform(delete("/api/customers/{id}", customer.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Customer> customers = customerRepository.findAll();
        assertThat(customers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
