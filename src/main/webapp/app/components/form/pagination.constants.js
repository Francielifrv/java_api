(function() {
    'use strict';

    angular
        .module('javaApiApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
