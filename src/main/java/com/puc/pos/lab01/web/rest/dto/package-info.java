/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.puc.pos.lab01.web.rest.dto;
