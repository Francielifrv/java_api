package com.puc.pos.lab01.repository;

import com.puc.pos.lab01.domain.Customer;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;

/**
 * Spring Data MongoDB repository for the Customer entity.
 */
@SuppressWarnings("unused")
public interface CustomerRepository extends MongoRepository<Customer,String> {


    Customer findByNameAndSiteAndCnpjAndAddress(String name, String site, String cnpj, String address);
}
